# -*- coding: utf-8 -*-
#******************************************************************************
#* SD - A framework for analyzing and visualizing complex software systems.
#* Copyright (C) 2005 - 2015 for all source codes:
#* Software Diagnostics Technology GmbH, Potsdam, Germany
#******************************************************************************

from __future__ import unicode_literals

from collections import deque
import csv
from fileiohelper import FileIoHelper


class CsvHeaderMissingError(ValueError):
    pass


class CsvColumnsUnmatchedError(ValueError):
    pass


def read_csv_header_and_rows(file_path):
    with CsvReader(file_path) as reader:
        return reader.header(), reader.rows()


def read_csv_rows(file_path):
    with CsvReader(file_path) as reader:
        return reader.rows()


CSV_DELIMITER = ';'


def _safe_csv_string(value):
    """
    Ensures that the provided string can be safely written to a CSV file:
    - string has neither newlines nor double-quotes
    """

    string = unicode(value)  # ensure unicode
    string = u' '.join(string.splitlines())  # remove all possible newlines
    string = string.replace(u'"', u"'")  # remove double quotes
    if u';' in string:
        string = u''.join((u'"', string, u'"'))  # add enclosing double quotes
    return string


class CsvRow(object):
    def __init__(self, fields=None):
        """
        Create a CsvRow.

        :param fields An iterable containing fields to be appended to the CsvRow.
        """
        # always copy input variable fields to be safe against modifications from outside.
        self._fields = deque(fields) if fields else deque()

    def append(self, value):
        self._fields.append(value)
        return self

    def extend(self, values):
        self._fields.extend(values)
        return self

    def prepend(self, value):
        self._fields.appendleft(value)
        return self

    def __iter__(self):
        return iter(self._fields)

    def __len__(self):
        return len(self._fields)

    def __getitem__(self, item):
        return self._fields[item]

    def __repr__(self):
        return unicode(self)

    def __unicode__(self):
        return CSV_DELIMITER.join(_safe_csv_string(field) for field in self._fields)


class CsvReader(object):
    """
        ``brief`` A CSV _reader which will iterate over lines in the CSV file with the given name.
    """
    class _UTF8Recoder(object):
        """
            ``brief`` Iterator that reads an decoded stream and reencodes the input to UTF-8
        """
        def __init__(self, file_handle):
            """
                ``param`` file_handle: the file has to be opened using codecs.open
            """
            self._file_handle = file_handle

        def __iter__(self):
            return self

        def next(self):
            return self._file_handle.next().encode('utf-8')

    def __init__(self, file_path):
        self._file_path = file_path
        self._file_handle = FileIoHelper.open_for_read(unicode(self._file_path))
        self._reader = csv.reader(
            self._UTF8Recoder(self._file_handle),
            dialect=csv.excel,
            delimiter=str(CSV_DELIMITER))

        self._header = None
        try:
            self._header = self._csv_row(self._reader.next())
        except StopIteration:
            self.close()
            raise CsvHeaderMissingError('The csv-file to be read is empty: {}'.format(
                self._file_path))

    def header(self):
        return self._header

    def row_iter(self):
        for read_result in self._reader:
            yield self._csv_row(read_result)

    def rows(self):
        return [row for row in self.row_iter()]

    def close(self):
        self._file_handle.close()

    @staticmethod
    def _csv_row(utf8_encoded_strs):
        return CsvRow(s.decode('utf-8') for s in utf8_encoded_strs)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_tb):
        self.close()


class CsvWriter(object):
    def __init__(self, file_path):
        self._file_path = file_path
        self._file_handle = FileIoHelper.open_for_write(unicode(self._file_path))
        self._header_text, self._num_columns = None, None

    def write_header(self, csv_row):
        if self._num_columns is not None:
            raise CsvHeaderMissingError('a csv header has already been written')

        self._header_text, self._num_columns = unicode(csv_row), len(csv_row)
        self._write(csv_row)

    def write_row(self, row):
        if self._num_columns is None:
            raise CsvHeaderMissingError('a csv header has not been written yet')

        if len(row) != self._num_columns:
            raise CsvColumnsUnmatchedError(
                'number of entries in row - {} - does not match with the written header - {}'
                .format(row, self._header_text))

        self._write(row)

    def close(self):
        self._file_handle.close()

    def _write(self, row):
        self._file_handle.write(unicode(row))
        self._file_handle.write('\n')

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_tb):
        self.close()
