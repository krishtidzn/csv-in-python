#******************************************************************************
#* SD - A framework for analyzing and visualizing complex software systems.
#* Copyright (C) 2005 - 2015 for all source codes:
#* Software Diagnostics Technology GmbH, Potsdam, Germany
#******************************************************************************

import codecs


def read_lines(file_path):
    return list(FileIoHelper.read(file_path))


class FileIoHelper(object):

    _ENCODING_TEST_BLOCKSIZE = 512 * 1024  # 512 kByte blocks
    _HEURISTIC_MAX_WORDSIZE = _ENCODING_TEST_BLOCKSIZE

    @classmethod
    def _test_encoding(cls, file_path, encoding):
        try:
            with codecs.open(file_path, encoding=encoding) as file_handle:
                while file_handle.read(cls._ENCODING_TEST_BLOCKSIZE):
                    pass
            return True
        except UnicodeError:
            return False

    @classmethod
    def _cp1252_file_might_be_utf_16(cls, file_path):
        with codecs.open(file_path, encoding='cp1252') as file_handle:
            # Heuristic: expect that at least one whitespace is contained
            #            in the first _HEURISTIC_MAX_WORDSIZE bytes
            #            == hoping to find ur'^\S*\s\x00'
            content = file_handle.read(cls._HEURISTIC_MAX_WORDSIZE).split()
            if len(content) == 1 or not content[1].startswith(u'\x00'):
                return False

            # Possible alternative implementation: hope to find
            # ur'\x00[\w\s]\x00[\w\s]\x00'

            # UTF-16 encoding sucks.

        return True

    @classmethod
    def detect_encoding(cls, file_path):
        """
        Tries to detect the character encoding of the specified file. Returns
        'None' if encoding could not be detected.

        Warning: Depending on the actual encoding, this might require to read
        the file multiple times.
        """

        # get header
        with open(file_path, 'rb') as file_handle:
            header = file_handle.read(4)

        # try to detect encoding based on byte-order-mark
        # HINT: do not change this to a map, because it is ordered!
        encodings = ((codecs.BOM_UTF32_BE, 'utf-32-be-bom'),
                     (codecs.BOM_UTF32_LE, 'utf-32-le-bom'),
                     (codecs.BOM_UTF16_BE, 'utf-16-be-bom'),
                     (codecs.BOM_UTF16_LE, 'utf-16-le-bom'),
                     (codecs.BOM_UTF8, 'utf-8-sig'), )
        for bom_signature, encoding in encodings:
            if header.startswith(bom_signature):
                # TODO: verify detected encoding (reading content might fail)
                return encoding

        # try UTF-8
        if cls._test_encoding(file_path, 'utf-8'):
            return 'utf-8'

        # try whether it is _not_ 'cp1252' -> fail, should work now
        if not cls._test_encoding(file_path, 'cp1252'):
            return None

        # try UTF-16 encodings
        if cls._cp1252_file_might_be_utf_16(file_path):
            for encoding in ('utf-16-le', 'utf-16-be'):
                if cls._test_encoding(file_path, encoding):
                    return encoding

        # use default encoding
        return 'cp1252'

    @classmethod
    def read(cls, file_path, encoding=None):
        """
        Generator that yields the content of the specified file line-by-line as
        unicode strings. The method only treats '\r', '\n' and '\r\n' as
        line-endings. These line-endings are removed from resulting strings.

        If you want a different behavior regarding line-endings, feel free to
        use 'file_handle' directly and iterate over the result.

        If 'encoding' is None, auto detection is tried.
        Encoding errors are ignored.
        """
        with cls.open_for_read(file_path, encoding) as file_handle:
            partial_line = None
            # looping over file objects also considers line-breaks
            # that we want to ignore. If they occur, we need to
            # temporarily store the content of the line until a 'real'
            # line-break is reached.
            for line_with_ends in file_handle:
                line = line_with_ends.rstrip(u'\r\n')

                # update partial line
                partial_line = partial_line + line if partial_line else line

                if len(line_with_ends) != len(line):
                    # line either ends with '\r', '\n', '\r\n' -> real line break -> yield
                    yield partial_line
                    partial_line = u''

            if partial_line is not None:
                # yield last line
                yield partial_line

    @classmethod
    def read_all(cls, file_path, encoding=None):
        """
        Returns the complete file content of the specified file as unicode
        string. The line endings '\r' and '\r\n' are unified to '\n'.

        If 'encoding' is None, auto detection is tried.
        Encoding errors are ignored.
        """
        return u'\n'.join(cls.read(file_path, encoding))

    @classmethod
    def write(cls, file_path, content):
        with cls.open_for_write(file_path) as file_handle:
            file_handle.write(content)

    @classmethod
    def _get_encoding(cls, file_path, encoding):

        if not encoding:
            encoding = cls.detect_encoding(file_path) or 'cp1252'

        if encoding.endswith('-le-bom') or encoding.endswith('-be-bom'):
            encoding = encoding[:-7]  # remove '-Xe-bom'

        return encoding

    @classmethod
    def open_for_read(cls, file_path, encoding=None):
        """
        Tries to open the specified file using the given encoding and returns
        the file handle. If 'encoding' is None, auto detection is tried.
        Encoding errors are ignored.

        The returned handle provides unicode strings (read access).
        """

        return cls._file_handle(file_path, 'rb', encoding)

    @classmethod
    def open_for_write(cls, file_path):
        """
        Tries to open the specified file using the given encoding and returns
        the file handle. If 'encoding' is None, auto detection is tried.
        Encoding errors are ignored.

        The returned handle provides unicode strings (write access).
        """
        return cls._file_handle(file_path, 'wb', 'utf-8')  # we always write utf-8 files

    @classmethod
    def _file_handle(cls, file_path, mode, encoding=None):
        encoding = cls._get_encoding(file_path, encoding)

        return codecs.open(file_path, mode=mode, encoding=encoding, errors='ignore')

    @classmethod
    def raw_file_handle_read(cls, file_path):
        """
        Tries to open the specified file using the given encoding and returns
        the file handle. If 'encoding' is None, auto detection is tried.
        Encoding errors are ignored.

        The returned handle provides raw strings decoded with the specified
        (or detected) encoding (read access).
        """

        encoding = cls._get_encoding(file_path, None)
        return codecs.EncodedFile(open(file_path), encoding, errors='ignore')
